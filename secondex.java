import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

import static java.lang.System.exit;

public class secondex {
    public static void main(String[] args) {
        /*one();*/
       /* two();*/
       /* three();*/
      /*  four();*/
    /*    five();*/
       /* six();*/
       /* seven();*/
       /* eight();*/
       /* nine();*/
        ten();

    }

    private static void one() {
        /*        Write an application that will show if entered value is greater, equal or lower than 30.*/
        System.out.println("Enter value");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        if (a > 30) {
            System.out.println("value greater 30");
        }
        if (a == 30) {
            System.out.println("value equal 30");
        }
        if (a < 30) {
            System.out.println("value lower 30");
        }

    }

    private static void two() {
       /*    As above but compare two values at the same time. Verify if first value is greater than 30 and
           second value is greater than 30, and so on.*/
        System.out.println("Enter values");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        String one="";
        String two="";
        String output="";
        if (a > 30) {
            one = "first value greater 30";
        }
        if (a == 30) {
            one = "first value equal 30";
        }
        if (a < 30) {
            one = "first value lower 30";
        }
        if (b > 30) {
            two = "second value greater 30";
        }
        if (b == 30) {
            two = "second equal 30";
        }
        if (b < 30) {
            two = "second lower 30";
        }
        output=one+" and "+two;
        System.out.println(output);


    }

    private static void three() {
        /*   As above but only one of the values has to be greater, equal or lower than 30.*/
        System.out.println("Enter values");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        String one="";
        String two="";
        String output="";
        if (a > 30) {
            one = "first value greater 30";
        }
        if (a == 30) {
            one = "first value equal 30";
        }
        if (a < 30) {
            one = "first value lower 30";
        }
        if (b > 30) {
            two = "second value greater 30";
        }
        if (b == 30) {
            two = "second equal 30";
        }
        if (b < 30) {
            two = "second lower 30";
        }
        output=one+" and "+two;
        System.out.println(output);
    }

    private static void four() {
  /*         Write an application that for any entered number between 0 and 9 will provide it’s name. For
           example for “3” program should print “three”.*/
        System.out.println("Enter value");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        switch (a) {
            case 0:
                System.out.println("Zero");
                break;
                case 1:
                System.out.println("one");
                    break;
                case 2:
                System.out.println("two");
                    break;
                case 3:
                System.out.println("three");
                    break;
                case 4:
                System.out.println("four");
                    break;
                case 5:
                System.out.println("five");
                    break;
                case 6:
                System.out.println("six");
                    break;
                case 7:
                System.out.println("seven");
                    break;
                case 8:
                System.out.println("eight");
                    break;
                case 9:
                System.out.println("nine");
                    break;

        }


    }

    private static void five() {
 /*          Using nested for loops draw (parents loop iterator should be called “row”,
           child – “column”):



           triangle,
*rectangle with diagonals,
**Christmas tree*/

for (int i=0; i<10;i++) {
    for (int n=0;n<10;n++) {
        System.out.print("*");
    }
    System.out.println();
}
    }

    private static void six() {
   /*        Write a simple application that will simulate a shopping. Use only if-else flow control.
           Consider following cases:


           If you would like to buy a bottle of milk – cashier will ask you for a specific amount of
           money. You have to enter that value and verify if it is same as the cashier asked.
                   If you would like to buy a bottle of wine – cashier will ask you if you are an adult and
           for positive answer ask for a specific amount of money.*/
   String milk = "Milk";
   int milkPrice = 2;
   int vinePrice = 10;
   int waterPrice = 5;
   int currentChoice = 0;
   int money;
   String water = "Water";
   String vine = "Vine";
        System.out.println("Hello!");
        System.out.println("What do you like?");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        if (answer.equalsIgnoreCase(vine)) {
            currentChoice=vinePrice;
            System.out.println("Please enter you age");
            int age = scanner.nextInt();
            if (age<18) {
                System.out.println("You too young!");
                exit(0);
            }
        }
        if (answer.equalsIgnoreCase(milk)) {
            currentChoice=milkPrice;
        }
        if (answer.equalsIgnoreCase(water)) {
            currentChoice=waterPrice;
        }
        if (!answer.equalsIgnoreCase(vine)&&!answer.equalsIgnoreCase(water)&&!answer.equalsIgnoreCase(milk)) {
            System.out.println("We dont have it");
            exit(0);
        }
        System.out.println("GIVE ME YOU MONEY!!!");
        money = scanner.nextInt();
        if (money<currentChoice) {
            System.out.println("YOU DONT HAVE MONEY!!");
        } else {
            System.out.println("thank you for the purchase");
        }

    }

    private static void seven() {
        /*   Write a “divide by” application. User should be able to enter initial value that will be divided
           in a loop by a new value entered by a user. Division should occur as long, as entered value
           will be different than 0. Result of division should be rounded to the fourth decimal point and
           printed to the console.*/
        System.out.println("Enter init value");
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        while (true) {
            System.out.println("Enter divisor");
            int b = scanner.nextInt();
            if (b==0) {
                break;
            } else {
                double c = (double) a/b;
                System.out.printf("%5.4f\n",c);
            }
        }
    }

    private static void eight() {
          /* Write a simple “echo” application, that will:



           print back entered string,
           go to the beginning of a loop if user will enter “continue”,
           break the loop with a “good bye!” message, if user will enter “quit”.*/
          boolean cont=true;

          Scanner scanner = new Scanner(System.in);
          while (cont) {
              String answer = scanner.nextLine();
              System.out.println(answer);
              System.out.println("Continue?");
              String contin = scanner.nextLine();
              if (contin.equalsIgnoreCase("continue")) {
                  cont=true;
              }
              if (contin.equalsIgnoreCase("quit")) {
                  System.out.println("Good bye!");
                  cont=false;
              }
          }
    }

    private static void nine() {
        /*   Write an application that will find biggest value within array of int variables.


           check your application using randomly generated array (use Random class),
           check your application at least 5 times in a loop (generate random array -> print
           array to the console -> find biggest value -> print biggest value -> manually verify
           results)*/
        int [] arr = new int [10];
        Random random = new Random();
        for (int i=0; i<arr.length; i++) {
            arr[i]=Math.abs(random.nextInt(100));
            System.out.print(""+arr[i]+" ");
        }
        System.out.println(Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println();
        System.out.println(Arrays.toString(arr));
        System.out.println(arr[arr.length-1]);
    }

    private static void ten() {
          /* Write an application that will find the longest char sequence within an array of type String.
           Test it in the same way as you have done in the previous exercise. How will you generate
           random char sequences?*/
    }

}
