package OOP;

import java.util.ArrayList;
import java.util.Arrays;

public class OOP {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(createProducts()));
    }

    public static Product[] createProducts () {
        Product[] products = new Product[5];
        for (Product product:products) {
            product = new Product();
        }
//one
        return products;
    }
}
