import java.util.*;

public class Basics {
    public static void main(String[] args) {
/*one();*/
        /*two();*/
       /* three();*/
       /* four();*/
       /* five();*/
        six();
    }

    private static void one () {
        /*Use System.out.print method to print the same statement in separate lines.*/
        System.out.print("Hello world\n");
        System.out.print("Hello world\n");
    }
    private static void two () {
        /*Enter any value with several digits after the decimal point and assign it to variable
        of type double. Display the given value rounded to two decimal places.*/
        double a = 2.346321;
        System.out.printf("%5.2f\n",a);
    }
    private static void three () {
        /*Display any three strings of characters on one line so that they are aligned to the right
        edge of the 15-character blocks. How to align strings to the left edge?*/
        String sampleText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt "
                + "ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris "
                + "nisi ut aliquip ex ea commodo consequat.";
        String spaces =" ";
        for (int i = 0; i<14; i++) {
            spaces=spaces+" ";
        }
        String textAlignRight = spaces+sampleText;
        System.out.println(textAlignRight);
        List <String> arrayText = new  ArrayList <String> ();
        for (int i = 0; i < textAlignRight.length(); i = i + 15)
        {
            int endindex = Math.min(i + 15, textAlignRight.length());
            arrayText.add(textAlignRight.substring(i, endindex));
        }
        arrayText.remove(0);
        String output="";
        for (String n:arrayText) {
            output=output+n;
        }
        System.out.println(output);
    }
    private static void four () {
       /* Enter two values of type int. Display their division casted to the double type and rounded to
        the third decimal point.*/
       int a=10;
       int b=3;
       double c = (double) a/b;
        System.out.println(c);
        System.out.printf("%5.3f",c);

    }
    private static void five () {
  /*      *Sum two integer variables initialized with maximal values for that type.*/
        int a = Integer.MAX_VALUE;
        int b = Integer.MAX_VALUE;
        int c = a+b;
        long d = (long) a+b;
        System.out.println(c);
        System.out.println(d);
        System.out.println(a);



    }private static void six () {
       /* Create three variables, one for each type: float, byte and char. Enter values corresponding to
        those types using Scanner. What values are you able to enter for each type?*/
       Scanner scanner = new Scanner(System.in);
       float a = scanner.nextFloat();
       byte b = scanner.nextByte();
       char c = scanner.next().charAt(0);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);


    }

}
